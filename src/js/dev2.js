function checkFloor() {
    var item_height = $('.floor__stroke').height();
    var top_correction = item_height * 2;
    var count = $('.floor__stroke').length;
    var button = '.floor__stroke';
    if ($(button).length) {
        $(document).on('click', button, function (e) {
            mainFunc($(this));
        });
        $(document).on('click',  '.etagirio__up' , function (e) {
            var el = $('.floor__stroke.active').prev();
            if(el.length === 0) return;
            mainFunc(el);
        });
        $(document).on('click', '.etagirio__down' , function (e) {
            var el = $('.floor__stroke.active').next();
            if(el.length === 0) return;
            mainFunc(el);
        });
        $(document).ready(function () {
            mainFunc($('.floor__stroke.active'));
        });
    } else {
        $(document).off('click', button);
    }
    function mainFunc(that) {
        var stroke = that;
        var element = $('.floor__stroke');
        var list = $('.floor__list');
        var position = stroke.index();
        var top_posisition;
        if (stroke.index() <= 2) {
              top_posisition = (stroke.index() * item_height);
        } else if ((stroke.index() < count) && (stroke.index() >= count - 2) ) {
              top_posisition = (stroke.index() * item_height);
        } else if ((stroke.index() >= 2) && (stroke.index() < count - 2)) {
             top_posisition = (stroke.index() * item_height);
        }
        list.animate({top: (-top_posisition + top_correction)}, 200);
        if (!stroke.hasClass('active')) {
            element.removeClass('active neighbour');
            stroke.addClass('active');
        }
        element.eq(position - 1).addClass('neighbour');
        element.eq(position + 1).addClass('neighbour');
        if(stroke.index() === 0){$('.etagirio__up').addClass('hide')}else{$('.etagirio__up').removeClass('hide')}
        if(stroke.index() === count - 1){$('.etagirio__down').addClass('hide')}else{$('.etagirio__down').removeClass('hide')}
    }

}
$(document).ready(function () {
    checkFloor();
});