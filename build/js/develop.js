var interlight;
function towerLight(){
    var koef = 10;
    var timing = 100;
    interlight = null;
    if($('.tp-screen-active.section--tower').length > 0){
        var l = $('.st0kno').length;
        for (var i = 0; i < l ; i++){
            logistics($('.st0kno')[i])
        }
        interlight = setInterval(function () {
            logistics($('.st0kno')[parseInt(Math.random()*l)]);

        },timing);
    }
    function lightOrNot(){
        return Math.random()*100 <= koef ? true : false;
    }
    function logistics(item){
        if(lightOrNot()){
            $(item).addClass('hidewindow');
        }else{
            $(item).removeClass('hidewindow')
        }
    }

}
var routeModeGoogleMap = false;
function routeModeActivate() {
    $('.maper__route-btn').addClass('active');

    routeModeGoogleMap = true;
}
function routeModeDisactivate() {
    $('.maper__route-btn').removeClass('active');
    routeModeGoogleMap = false;
}
function initMap(){
    var myOptions = {
        zoom: zoomMap,
        scrollwheel: false,
        center: new google.maps.LatLng(centerMap.lat, centerMap.lng),
        disableDefaultUI: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        styles:[
            {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ecdbcf"
                    }
                ]
            }
        ]
    }
    var pins = [];
    var infos = []
    var map = new google.maps.Map(document.getElementById('map'), myOptions);
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        polylineOptions: {
            strokeColor: "#d48069"
        }
    });
    var googleGeocoder = new google.maps.Geocoder();
    var endpos =  getForRoute(finalDestination);
    var onChangeHandler = function() {
        var trim = document.getElementById('startpos').value.replace('(', '').replace(')', '');
        var start = trim.split(',');
        var res = {lat:parseFloat(start[0].trim()), lng:parseFloat(start[1].trim())}
        calculateAndDisplayRoute(  directionsDisplay, directionsService,res , endpos, true);
    };
    for (var i = 0 ; i < markers.length; i++){
        addMarker(markers[i]);
    };
    map.addListener('click', function(e) {
        if(routeModeGoogleMap) calculateAndDisplayRoute(directionsDisplay, directionsService, e.latLng , endpos);
    });
    $(document).on('change', '#startpos-styler', onChangeHandler);
    $(document).on('change' , '.maper__navinput', onchangeFilter);
    $(document).ready(function () {
        onchangeFilter();
    });
    function addToSelectNewPOsition(latLng){
        var select = document.getElementById('startpos');
        var option = document.createElement('option');

        googleGeocoder.geocode(  { 'latLng': latLng }, function( results, status ) {
             option.innerText = results[0].formatted_address ;
            option.value = latLng;
            select.prepend(option);
            setTimeout(function() {
                $(select).trigger('refresh');
            }, 1)
        });

    }
    function setInfo(info) {
        $('.maper__route-info').addClass('active');
        $('.maper__route-info p>span').text(info.start_address);
        $('.maper__route-info p>b').text(info.distance.text+' '+info.duration.text+' на машине');
    }
    function getForRoute(obj) {
        return new google.maps.LatLng(obj.lat, obj.lng)
    }
    function calculateAndDisplayRoute(directionsDisplay, directionsService, startpos , endpos, ischanged) {
        $('.maper__route-select').addClass('active');
        directionsService.route({
            origin: startpos,
            destination: endpos,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var info = response.routes[0].legs[0];

                if (!ischanged)addToSelectNewPOsition(startpos);
                setInfo(info);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
    function onchangeFilter() {
        $('.maper__navinput').each(function () {
            var type = $(this).attr('data-type');
            var show = $(this).prop('checked');
            for (var i = 0 ; i < pins.length; i++){
                if(pins[i].type == type){
                    pins[i].pin.setVisible(show);
                }
            }

        })
    }
    function addMarker(marker){
        var contentString = '<div class="marker-test"><h3>'+ marker.title +'</h3><p>'+ marker.text +'</p></div>';
        var infowindow = new google.maps.InfoWindow({ content: contentString });
        infos.push(infowindow);
        var pin = new google.maps.Marker({
            position: new google.maps.LatLng(marker.lat, marker.lng),
            map: map,
            icon: 'images/pin/'+marker.url,
        });
        var ctrl = {
            type:marker.type,
            pin:pin
        }
        pins.push(ctrl);
        google.maps.event.addListener(pin, 'click', function() {
            for(var gf = 0; gf < infos.length; gf++) {
                infos[gf].close();
            }
            infowindow.open(map,pin);
        });
    }
}
function startAnimation(){
    $('.firstscreen__text').addClass('animate');
    $('.section--tower').addClass('animate');
    $('.section--views').addClass('animate');
    $('.troll-counter').trollCounter();
}
function tripleItem(){
    var  elem = '.triple__item';
    if($(elem).length){
        $(document).on('click', elem , function () {
            if(!$(this).hasClass('hover')) {
                $(elem).removeClass('hover');
                $(this).addClass('hover');
                $('.triple').addClass('hover');
            }
        });
    }else{
        $(document).off('click', elem);
    }
}
function doubleColorAside() {
    $(document).ready(function (){
        Stickyfill.add($('.stickey__wrap'));
        changeAsideColor();
    });
    window.addEventListener('scroll', changeAsideColor);
}
function changeAsideColor(){
    if($('.stickey__b').length > 0 && window.innerWidth > 992){
        var st = $(window).scrollTop();
        var wh = window.innerHeight;
        var screenBotom = wh + st;
        var hm = $('.stickey__a .sidebar').outerHeight();
        var checker = $('.js-aside-height-checker');
        for (var i = 0; i < checker.length; i++ ){
            var check = checker[i];
            var checkerPos = $(check).offset().top;
            if(checkerPos > st && checkerPos < screenBotom  ){
                var rest = checkerPos - hm;
                if($(check).hasClass('reverse')){
                    $('.stickey__b').css({'bottom':'auto', 'top': 0});
                    $('.stickey .sidebar--white').css({'bottom':'auto', 'top': 0});
                    $('.stickey__b').height(hm - (screenBotom - checkerPos) );

                }else{

                    $('.stickey__b').css({'bottom':0, 'top': 'auto'});
                    $('.stickey .sidebar--white').css({'bottom':0, 'top': 'auto'});
                    $('.stickey__b').height((st > rest && ($('.js-aside-height-checker').css('position') !== "absolute")? st - rest : 0));

                }

            }else if (checkerPos < st && checkerPos > (st - wh)){
                var rest = checkerPos - hm;
                $('.stickey__b').height((st > rest && ($('.js-aside-height-checker').css('position') !== "absolute")? st - rest : 0));

            }else if(st == 0){$('.stickey__b').height(0);}
        }
        $('.stickey__a .sidebar').scroll(function () { $('.stickey__b .sidebar').scrollTop($(this).scrollTop());  });
        $('.stickey__b .sidebar').scroll(function () { $('.stickey__a .sidebar').scrollTop($(this).scrollTop());  });
    }
}
function butter() {
    $(document).on('click', '.sidebar__butter', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.sidebar__nav').stop().hide();
        }else{
            $(this).addClass('active');
            $('.sidebar__nav').stop().show();
        }
        if($('.stickey').length > 0)changeAsideColor();
    })
}
var parallaxInstance = [];
function checkForSlide() {
    var settings = {
        animationTime: 2000,
        infinite: false
    };
    var lastAnimation;
    var slider = $('.js-slider-screen');
    if (slider.length > 0 ){
        window.scrollTop = 0;
        $('.js-slider-screen > *').addClass('tp-screen').eq(0).addClass('tp-screen-active');
        $(document).bind('mousewheel DOMMouseScroll', ishandler);
        var resize = debounce(function() {
            $(document).bind('mousewheel DOMMouseScroll', ishandler);
        }, 250);
        window.addEventListener('resize', resize);
    }
    function ishandler(event) {
        if(window.innerWidth > 992 && $('html').hasClass('desktop')){
            event.preventDefault();
            var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;
            initScroll(event, delta);
        }else{
            $(document).unbind('mousewheel DOMMouseScroll', ishandler);
        }
    }
    function initScroll(event, delta) {
        var deltaOfInterest = delta,
            timeNow = new Date().getTime(),
            quietPeriod = 500;

        if(timeNow - lastAnimation < quietPeriod + settings.animationTime) {
            event.preventDefault();
            return;
        }
        if (deltaOfInterest < 0) {
            moveNext();
        } else {
            movePrev();
        }
        lastAnimation = timeNow;
    }
    function moveNext() {
        var el = $('.tp-screen-active');
        var next = el.next();
        if(el.hasClass('section--tower')){
            el.removeClass('tp-screen-active');

            setTimeout( function (args) {
                next.addClass('tp-screen-active');
                afterShow();
            } , 1500);
        }else if(next.length > 0){
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            next = $('.tp-screen:first-child');
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow ,100);
        }
    }
    $(document).on('click','.downbutton', moveNext);
    function movePrev() {
        var el = $('.tp-screen-active');
        var prev =   el.prev();
        if(prev.length > 0){
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            prev = $('.tp-screen:last-child');
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }
    }
    function afterShow() {
        towerLight();
        if($('.tp-screen-active').hasClass('section--map') || $('.tp-screen-active').hasClass('infobox')){
            doAsideWhite();
            $('.downbutton').hide();
            $(document).unbind('mousewheel DOMMouseScroll', ishandler);
            $(document).bind('mousewheel DOMMouseScroll', isonmap);
            $('.sometime-hide').addClass('active');
            fromdown = 0 ;

        }else{
            doAsideNotWhite();
            $('.downbutton').show();
        }
    }
    var fromdown = 0;
    function isonmap(event){

        var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;
        if (delta > 0 && window.scrollY == 0){
            if(fromdown > 10) {
                movePrev();
                $(document).unbind('mousewheel DOMMouseScroll', isonmap);
                setTimeout(function () {
                    $(document).bind('mousewheel DOMMouseScroll', ishandler);
                }, 500);

                $('.sometime-hide').removeClass('active');
            }
            fromdown++;
        }
    }
}
function doAsideWhite() {
    $('.sidebar').addClass('sidebar--white');
    $('.sidebar .btn').addClass('btn--inverted');
    $('.sidebar__butter').addClass('sidebar__butter--white');
}
function doAsideNotWhite() {
    $('.sidebar').removeClass('sidebar--white');
    $('.sidebar .btn').removeClass('btn--inverted');
    $('.sidebar__butter').removeClass('sidebar__butter--white');
}
function dataDropdown(){
    var btn = '[data-dropdown-btn]';
    if(btn.length){
        $(document).on('click', btn, clickHandler);
    }else{
        $(document).off('click', btn, clickHandler);
    }
    function clickHandler(){
        var id = $(this).attr('data-dropdown-btn');
        if(!$(this).hasClass('active')){
            openDD(id);
        }else{
            closeDD(id);
        }
    }
    var close = '[data-dropdown-close]';
    if(close.length){
        $(document).on('click', close, closeHandler);
    }else{
        $(document).off('click', close, closeHandler);
    }
    function closeHandler(){
        var id = $(this).attr('data-dropdown-close');
        if(!$(this).hasClass('active')){
            closeDD(id);
        }
    }
}
function openDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.addClass('active');
    box.addClass('active');
    if(id == 'id01') {$("body, html").animate({scrollTop: $('[data-dropdown-box=id01]').offset().top}, 500);}
    if(id == 'id02') {$("body, html").animate({scrollTop: $('[data-dropdown-box=id02]').offset().top - 80}, 500);}
}
function closeDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.removeClass('active');
    box.removeClass('active');
}
function mobfloor() {
    var box = $('.mobfloor input:checked').closest('label')[0];
    var container  = $('.mobfloor')[0];
    container.scrollTop = box.offsetTop - container.offsetHeight/2 + box.offsetHeight/2;
}
function generateTextFromSVG(){
    var els = $('.planner__text') ;
    var box = $('.planner__svg-info');
    els.each(function () {
        var div = document.createElement('div');
        div.setAttribute('data-id', $(this).closest('g').find('.planner__flat').attr('data-id'));
        $(div).addClass('planner__svg-item');
        var p = $(this).find('tspan');
        for(var i = 0; i < p.length; i++){
            $(div).append('<p>'+$(p[i]).text()+'</p>');
        }
       box.append(div);
    });
}
function dragerIntrior() {
    if($('.js-x-dragabble').length){
        var dragelement = document.getElementsByClassName('js-x-dragabble')[0];
        function initDrag(){
            dragelement.removeEventListener('mousedown', mDown);
            dragelement.removeEventListener('touchstart', mDown);
            var container = document.getElementsByClassName('interior__a')[0];
            var koef = (window.innerWidth - $('.sidebar').width())/2;
            var leftText = $('.interior__text-a');
            var rightText = $('.interior__text-b');
            var leftOp = $('.interior__a .interior__bg');
            var rightOp = $('.interior__b .interior__bg');
            container.style.width =  koef + 'px';
            dragelement.style.left = (container.offsetWidth - 50) + "px";
            var data = {
                dragstart: false,
                startX: 0,
                initwidth: container.offsetWidth,
                initX: 50,
                initY:  window.innerWidth > 992 ? window.innerWidth - $('.sidebar')[0].offsetWidth - 65 : window.innerWidth -65
            };
            dragelement.addEventListener('mousedown', mDown, false);
            dragelement.addEventListener('touchstart', mDown, false);
            function mDown (e) {

                data.startX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
                data.dragstart = true;
                data.initwidth = container.offsetWidth;
                document.addEventListener('touchmove', mMove, false);
                document.addEventListener('mousemove', mMove, false);
                document.addEventListener('touchend', mUp, false);
                document.addEventListener('mouseup', mUp, false);
            }
            function mMove(e) {
                var curentX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
                dragElement(curentX - data.startX);

            }
            function mUp() {
                document.removeEventListener('touchmove', mMove);
                document.removeEventListener('mousemove', mMove);
                document.removeEventListener('touchend', mUp);
                document.removeEventListener('mouseup', mUp);
            }
            function dragElement(shift) {
                var res = data.initwidth + shift;
                var op = res - koef;
                leftOp[0].style.opacity = 0.5 + (op * 0.5)/koef;
                rightOp[0].style.opacity = 0.5 - (op * 0.5)/koef;
                leftText[0].style.opacity =  1 + (op / koef)*2.3;
                rightText[0].style.opacity =  1 - (op / koef)*2.3;
                if (res < data.initX) res = data.initX;
                if (res > data.initY) res = data.initY;
                dragelement.style.left = (res - 50) + "px";
                container.style.width =  res + 'px';
            }
        }
        initDrag();
    }
}
function changerInterior() {
    var selector = '.interior__arrow';
    if($(selector).length > 0){
        $(document).on('click', selector, function () {
            if($('.interior__selected--a').hasClass('active')){

                $('.interior__selected--a').removeClass('active');
                $('.interior__seltext-a').removeClass('active');
                $('.interior__a').removeClass('active');
                $('.interior__b').addClass('active');
                $('.interior__seltext-b').addClass('active');
                $('.interior__selected--b').addClass('active');
            }else{
                $('.interior__selected--a').addClass('active');
                $('.interior__selected--b').removeClass('active');
                $('.interior__seltext-b').removeClass('active');
                $('.interior__seltext-a').addClass('active');
                $('.interior__b').removeClass('active');
                $('.interior__a').addClass('active');
            }
        });
    }

}
function gallerySlider() {
    var elements = $('.gallery__simple-slider');
    if (elements.length > 0){
        elements.slick({
            dots:false,
            arrows:false
        });
        $('.gallery__simple-slider2').slick({
            dots: false,
            arrows: false,
            slidesToShow: 3,
            infinite: true,
            responsive: [
                {  breakpoint: 768,  settings: { slidesToShow: 1, } }
                ]
        });
        $('.gallery__simple-slider3').slick({
            dots: false,
            arrows: false,
            slidesToShow: 2,
            infinite: true,
            responsive: [
                {  breakpoint: 768,  settings: { slidesToShow: 1, } }
            ]
        });
        $(document).on('click', '.gallery__arrow--prev', function () {
            getInstance(this).slick('slickPrev');
        });
        $(document).on('click', '.gallery__arrow--next', function () {
            getInstance(this).slick('slickNext');
        });
        $(document).on('click', '.gallery__pin', function () {
            $(this).siblings('.gallery__pin').removeClass('active');
            getInstance(this).slick('slickGoTo', $(this).data('slide') - 1);
            $(this).addClass('active');
        });
        $(document).on('click', '.gallery__nav-pin', function () {
            $('.gallery__nav-pin').removeClass('active');
            $('.gallery__slider').removeClass('active').eq($(this).index()).addClass('active');
            $(this).addClass('active');
            moveNav()
        });
        elements.on('afterChange', function(event, slick, currentSlide){
            slick.$slider.closest('.gallery__slider').find('.gallery__pin').removeClass('active').eq(currentSlide).addClass('active');
        });
        $('.gallery__simple-slider2').on('afterChange', function(event, slick, currentSlide){
            slick.$slider.closest('.gallery__slider').find('.gallery__pin').removeClass('active').eq(currentSlide).addClass('active');
        });
        $('.gallery__simple-slider3').on('afterChange', function(event, slick, currentSlide){
            slick.$slider.closest('.gallery__slider').find('.gallery__pin').removeClass('active').eq(currentSlide).addClass('active');
        });
    }
    function getInstance(element) {
       return $(element).closest('.gallery__slider').find('.js-instance');
    }

}
function moveNav() {
    var scrollHeight =  $('.gallery__nav-wrap')[0].scrollHeight;
    var boxHeight = $('.gallery__nav').height();
    var minHeight = $('.gallery__nav-pin').not('.active').outerHeight(true);
    var activeOffset = $('.gallery__nav-pin.active')[0].offsetTop;
    if(scrollHeight > boxHeight){
        var offset = 0;
        if(activeOffset > minHeight){offset = activeOffset - minHeight;}
        if((scrollHeight - boxHeight) < offset) {offset = (scrollHeight - boxHeight)}
        $('.gallery__nav-wrap').stop().animate({top:'-'+offset+'px'},200);
    }
}
function scrollAnimate(){
    var bottomCheck = $(window).height()+$(window).scrollTop();
    var windowTop = $(window).scrollTop()+($(window).height()/1.5);
    $('.js-anim__section').each(function(){
        if(windowTop>$(this).offset().top || bottomCheck > $('body').height()*0.98){
            var itemSect = $(this);
            var point = 0;
            itemSect.find('.js-anim__it').addClass('animated');
            var timer = setInterval(function(){
                itemSect.find('.js-anim__delay').eq(point).addClass('animated');
                point++;
                if(itemSect.find('.js-anim__delay').length == point){
                    clearInterval(timer);
                }
            },300);


        }
    });
}
function viewsLogic(){
    if($('#drag_panel').length > 0){
        function setHoverBox() {
            $('[data-hoverbox]').remove();
            var set = $('[data-hoverset]');
            var box = $('.js-panorama');
            if(set.length > 0){
                set.each(function () {
                    var div = document.createElement('div');
                    $(div).addClass($(this).attr('data-time'));
                    div.setAttribute('data-hoverbox', $(this).attr('data-hoverset'));
                    div.setAttribute('style', 'left:'+parseInt($(this).offset().left)+'px; top:'+parseInt($(this).offset().top - $('.js-panorama').offset().top)+'px; width:'+parseInt($(this).width())+'px; height:'+parseInt($(this).height())+'px;');
                    box.prepend(div);
                });
            }
        }
        function getHover() {
            $(document).on('mouseover', '[data-hoverbox]', function(){
                $('.views__fon').addClass('hide');
                $('.active [data-hoverset="'+$(this).attr('data-hoverbox')+'"]').closest('.play').addClass('hover');
            })
            $(document).on('mouseleave', '[data-hoverbox]', function(){
                $('.views__fon').removeClass('hide');
                $('.active [data-hoverset="'+$(this).attr('data-hoverbox')+'"]').closest('.play').removeClass('hover');
            })
        }
        function changeDay() {
            var element = '.js-change-day';
            if ($(element).length) {

                $(document).on('click', element, function () {
                    if ($(this).hasClass('day')) {
                        $('.js-change-day').removeClass('active');
                        $('.picture').removeClass('active');
                        $(this).addClass('active');
                        $('.picture.day').addClass('active');
                        $('.js-panorama').removeClass('night');
                        $('.js-panorama').addClass('day');
                    }else{
                        $('.js-change-day').removeClass('active');
                        $('.picture').removeClass('active');
                        $(this).addClass('active');
                        $('.picture.night').addClass('active');
                        $('.js-panorama').addClass('night');
                        $('.js-panorama').removeClass('day');
                    }
                    reinitDrag();
                });
            } else {
                $(document).off('click', element);
            }
        }
        var data = {
            dragstart: false,
            startX:0,
            currentX:0,
            width:$('.picture.active').width()
        };
        var dataEye = {
            dragstart: false,
            startX:0,
            currentX:0,
            width: $('.indicator').width()
        };
        var panel = document.getElementById('drag_panel');
        var container = document.getElementById('action_window');
        var indicator = $('.indicator__button');
        function reinitDrag(){
            panel.removeEventListener('mousedown', mouseDown);
            panel.removeEventListener('touchstart', mouseDown);
            var clone = $('.picture.active image').attr('xlink:href');
            var img = document.createElement('img');
            $(img).attr('src', clone);
            $(img).attr('style', 'position:absolute; left: 9999%; max-width: none;');
            document.body.appendChild(img);
            img.onload = function () {
                $('#action_window').width(img.width);
                panel.addEventListener('mousedown', mouseDown, false);
                panel.addEventListener('touchstart', mouseDown, false);
                initBottomControl();
                $(img).remove();
            }

        }
        function initBottomControl() {
            indicator[0].removeEventListener('mousedown', mouseDownEye);
            indicator[0].removeEventListener('touchstart', mouseDownEye);
            indicator[0].addEventListener('mousedown', mouseDownEye, false);
            indicator[0].addEventListener('touchstart', mouseDownEye, false);
            var allpanelWidth = $('.indicator').width();
            var imgWidth = $('.picture.active').width();
            var windowWidth = $(window).width();
            var indicatorWidth = (allpanelWidth * windowWidth) / imgWidth;
            indicator.width(indicatorWidth);
        }
        function mouseDownEye(e) {
            if ( e.button > 0) return false;
            dataEye.startX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
            document.addEventListener('touchmove', mouseMoveEye, false);
            document.addEventListener('mousemove', mouseMoveEye, false);
            document.addEventListener('touchend', mouseUp, false);
            document.addEventListener('mouseup', mouseUp, false);
        }
        function mouseMoveEye(e) {
            var curentX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
            var difEye = curentX - dataEye.startX;
            var allpanelWidth = $('.indicator').width();
            var imgWidth = $('.picture.active').width();
            var dif = (difEye * imgWidth) / allpanelWidth;
            dragElement(dif * -1);
            dragElementEye(difEye);
        }
        function mouseDown(e) {
            if ( e.button > 0) return false;
            data.startX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
            document.addEventListener('touchmove', mouseMove, false);
            document.addEventListener('mousemove', mouseMove, false);
            document.addEventListener('touchend', mouseUp, false);
            document.addEventListener('mouseup', mouseUp, false);
        }
        function mouseMove(e) {
            var curentX = e.pageX === undefined ? e.targetTouches[0].pageX : e.pageX;
            var dif = curentX - data.startX;
            var allpanelWidth = $('.indicator').width();
            var imgWidth = $('.picture.active').width();
            var difEye = ((dif * allpanelWidth) / imgWidth) * -1;
            dragElement(dif);
            dragElementEye(difEye);
        }
        function mouseUp() {
            document.removeEventListener('touchmove', mouseMove);
            document.removeEventListener('mousemove', mouseMove);
            document.removeEventListener('touchend', mouseUp);
            document.removeEventListener('mouseup', mouseUp);
            document.removeEventListener('touchmove', mouseMoveEye);
            document.removeEventListener('mousemove', mouseMoveEye);
            data.currentX = parseInt(container.style.left);
            dataEye.currentX = parseInt(indicator[0].style.left);
        }
        function dragElement(shift) {
            var res = data.currentX + shift;
            if(res > 0 ) res = 0;
            if(res < ($(container).width() - window.innerWidth)*-1) res = ($(container).width() - window.innerWidth)*-1;
            container.style.left =  res + 'px';
        }
        function dragElementEye(shift) {
            var res = dataEye.currentX + shift;
            var maxRight = $('.indicator').width() - indicator.width();
            if(res < 0 ) res = 0;
            if(res > maxRight) res = maxRight;
            indicator[0].style.left =  res + 'px';
        }
        function getView() {
            var selector = '[data-get-floor]';
            if($(selector).length > 0){
                $(document).on('click', selector, function (e) {
                    e.preventDefault();
                    var url = $(this).attr('data-get-floor') + '.php';
                    var timing = 500;
                    $('.js-panorama').addClass('hide');
                    setTimeout(function () {
                        $.ajax({
                            url:url,
                            success: function (res) {
                                $('.views__line--right').html(res);
                                $('.js-change-day.night').removeClass('active');
                                $('.js-change-day.day').addClass('active');
                                $('.js-panorama').removeClass('night');
                                $('.js-panorama').addClass('day');
                                $('.js-panorama').removeClass('hide');
                                setTimeout(function () {
                                    container.style.left = 0;
                                    data = {
                                        dragstart: false,
                                        startX:0,
                                        currentX:0,
                                        width:$('.picture.active').width()
                                    };
                                    setHoverBox();
                                    reinitDrag();
                                },200);
                            }
                        });
                    },timing);

                })
            }
        }
        $(document).ready(function () {
            getView();
            setHoverBox();
            reinitDrag();
            getHover();
            changeDay();
        });
        var forresize = debounce(function () {
            setHoverBox();
        }, 100);
        window.addEventListener('resize', forresize);
    }
}
function afterHidePreloader(){
        $('.preloader-tr').remove();
        startAnimation();
        $('.sidebar').removeClass('hide');
        scrollAnimate();
        setTimeout(function () {
            $('.planner__svg').removeClass('hide');
        }, 700);
}
function newsTags() {
    var selector = '.newslist__tag input:not(.all)';
    if($(selector).length > 0){
        $(document).on('click', selector, function () {
            var i = 0;
            $(selector).each(function () {
                if($(this).prop('checked')){
                    $('.newslist__tag input.all').prop('checked', false).removeAttr('checked');
                    i++;
                }
            })
            if(i === 0){
                $('.newslist__tag input.all').prop('checked', true).attr('checked', 'checked');
            }
        })
    }

}
function plansItem() {
    var selector = '.plans__item';
    if($(selector).length > 0){
        $(document).on('click', selector, function () {
            $(selector).removeClass('active');
            $('.plans__bimg').removeClass('active');
            $(this).addClass('active');
            $('.plans__bimg').eq($(this).index()).addClass('active');

        })
    }
}
function scrollUpDown() {
    var elems = $('.js-up-down');
    var trans = 150;
    if(elems.length > 0 && window.innerWidth > 992){
        var scroller = function () {
            scrollCheck();
        };
        var inited = false;
        window.addEventListener('scroll', scroller);
        function scrollCheck(){
            var st = window.scrollY;
            var wh = window.innerHeight;
            var midtop = (st + wh * 0.4);
            var midbot = (st + wh * 0.8);
            if(inited){
                for(var i = 0; i < elems.length; i++){
                    var elTop =  $(elems[i]).data('data-top');
                    if( elTop < midbot && elTop > midtop ){
                        var kof = (midbot - elTop)/(wh * 0.4);
                        var res = trans * kof;
                        shift($(elems[i]), res.toFixed(0));
                    }
                }
            }else{
                setTimeout(function () {
                    for(var i = 0; i < elems.length; i++){
                        var top = $(elems[i]).offset().top;
                        $(elems[i]).data('data-top',top);
                        if(top < midtop) shift($(elems[i]), trans);
                    }
                    inited = true;
                },10);
            }
        }
        function shift(elem, shift) {
            elem.attr('style','transform: translateY(-'+shift+'px)');
        }
    }
}
viewsLogic();
doubleColorAside();
$(document).ready(function(){
    plansItem();
    scrollUpDown();
    newsTags();
    gallerySlider();
    dataDropdown();
    tripleItem();
    dragerIntrior();
    changerInterior();
    generateTextFromSVG();
    if( $('.preloader-tr').length > 0 ){
        setTimeout(function () {
            if($('.st0kno').length > 0) towerLight();
            if($('#vid').length > 0){
                document.getElementById('vid').play();
            }
            $('.preloader-tr').addClass('animate');
            var preloadTiming = $('.preloader-tr').hasClass('preloader-tr__second')? 2500 : 6000;
            setTimeout(function () {
                afterHidePreloader();
            },preloadTiming);
        }, 2000);

    }else{
        if($('.st0kno').length > 0) towerLight();
        if($('#vid').length > 0){
            document.getElementById('vid').play();
        }
        afterHidePreloader();
    }
    var scene2 = $('.js-parasector');
    for (var ir = 0; ir < scene2.length; ir++){
        var irp  = new Parallax(scene2[ir],{scalarY : 0});
        parallaxInstance.push(irp);
    }
    if($('#map').length){ initMap();}
    if($('.js-styler').length){ $('.js-styler').styler()}
    butter();
    checkForSlide();
    $('body').removeClass('hide');
});
var resizer = debounce(function() {
    changeAsideColor();
    dragerIntrior();
    if(window.innerWidth > 992) $('.sidebar__nav').removeAttr('style');
    if(window.innerWidth <= 992) $('.stickey__b').removeAttr('style');
}, 250);
var scroller = debounce(function () {
    scrollAnimate();
}, 100);
window.addEventListener('resize', resizer);
window.addEventListener('scroll', scroller);
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
// svg sprite
(function(window, document) {
    'use strict';
    var file = 'images/sprite.svg'; // путь к файлу спрайта на сервере

    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) return true;
    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        insertIT = function() {
            document.body.insertAdjacentHTML('afterbegin', data);
        },
        insert = function() {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };
    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }
    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }

            }
        }
        request.send();
    } catch (e) {}
}(window, document));
$.fancybox.defaults.clickSlide = "close";

